# assembly-spades-workflow-gf2

This geneflow2 workflow takes paired-end FASTQ files and performs de novo assembly on them. 

Inputs
------
1. Forward read file (FASTQ)
2. Reverse read file (FASTQ)

Outputs
-------
2. SPAdes output directory including contigs.fasta and scaffold.fasta

Apps Documentation
------------------
Documentation for the individual apps are in their respective directories:

| https://git.biotech.cdc.gov/geneflow-apps/spades-assembly-3.13.0-gf2.git   
